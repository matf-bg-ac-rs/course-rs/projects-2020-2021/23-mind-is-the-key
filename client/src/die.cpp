#include <cstdlib>
#include <ctime>
#include <headers/die.h>

Die::Die()
{
	sides = 6;
}

int Die::roll_die()
{
	int roll;
	int min = 1;
	unsigned seed;
	seed = time(0);
	srand(seed);

	roll = rand() % (sides - min + 1) + min;
	return roll;
}
