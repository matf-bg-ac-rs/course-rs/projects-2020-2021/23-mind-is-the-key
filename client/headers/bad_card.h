#ifndef BADCARD_H
#define BADCARD_H
#include "headers/card.h"

#include <QString>
#include <iostream>
#include <string>
#include <vector>

enum class BadType { LoseMoney, LoseAll, HalfMoney, MoveBackwards };

class BadCard : public Card
{

public:
	BadCard(const std::string &name, const std::string &text, const std::vector<BadType> &types);

	~BadCard();

	void set_card_text(std::string text) override;
	std::string get_card_text() override;
	std::string Type() const;
	std::string show() const override;

private:
	std::vector<BadType> card_types;
	std::string card_text;
};

#endif // BADCARD_H
